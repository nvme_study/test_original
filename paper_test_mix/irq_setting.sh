#!/bin/bash

service irqbalance stop
sysv-rc-conf --level 0123456S irqbalance off

for ((index = 0 ; index < "$1"; index++))
do
    irq_num=`grep -wR nvme0q"$index" /proc/interrupts | awk -F: '{print $1}' | awk '{print $1}'`
    cat /proc/irq/"$irq_num"/smp_affinity_list
done

for ((index = 0 ; index < "$1"; index++))
do
    irq_num=`grep -wR nvme0q"$index" /proc/interrupts | awk -F: '{print $1}' | awk '{print $1}'`
    if (( $index == 0 ))
    then
        core_id=0
    else
        core_id=`expr $index - 1`
        core_id=`expr $core_id / 2`
    fi
    
    echo "Queue $index -> Core $core_id"
    echo $core_id > /proc/irq/"$irq_num"/smp_affinity_list
done

for ((index = 0 ; index < "$1"; index++))
do
    irq_num=`grep -wR nvme0q"$index" /proc/interrupts | awk -F: '{print $1}' | awk '{print $1}'`
    cat /proc/irq/"$irq_num"/smp_affinity_list
done

