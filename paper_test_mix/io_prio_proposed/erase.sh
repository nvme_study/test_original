#!/bin/bash

rm -f -R "$1"/qd_1_random_read/repeat_test_*
rm -f -R "$1"/qd_1_random_read/total_*

rm -f -R "$1"/qd_1_random_write/repeat_test_*
rm -f -R "$1"/qd_1_random_write/total_*

rm -f -R "$1"/qd_1_seq_write/repeat_test_*
rm -f -R "$1"/qd_1_seq_write/total_*

rm -f -R "$1"/qd_4_random_read/repeat_test_*
rm -f -R "$1"/qd_4_random_read/total_*

rm -f -R "$1"/qd_4_random_write/repeat_test_*
rm -f -R "$1"/qd_4_random_write/total_*

rm -f -R "$1"/qd_4_seq_write/repeat_test_*
rm -f -R "$1"/qd_4_seq_write/total_*

rm -f -R "$1"/qd_4_mix_random/repeat_test_*
rm -f -R "$1"/qd_4_mix_random/total_*

rm -f -R "$1"/qd_64_random_read/repeat_test_*
rm -f -R "$1"/qd_64_random_read/total_*

rm -f -R "$1"/qd_64_random_write/repeat_test_*
rm -f -R "$1"/qd_64_random_write/total_*

rm -f -R "$1"/qd_64_seq_write/repeat_test_*
rm -f -R "$1"/qd_64_seq_write/total_*
