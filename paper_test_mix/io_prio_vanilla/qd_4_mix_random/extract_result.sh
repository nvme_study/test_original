#!/bin/bash

if (( $1 == 0))
then
#for int_agg in 0x0000 0x0202 0x0303 0x0404 0x0505 0x4040
    for int_agg in 0x0000
    do
        cat at_same_time_execution_"$2"/w_agg_"$int_agg"/4core_iops_125 >> data_result_same_time_"$2"_iops.dat
        cat at_same_time_execution_"$2"/w_agg_"$int_agg"/4core_iops_115 >> data_result_same_time_"$2"_iops.dat
#cat at_same_time_execution_"$2"/w_agg_"$int_agg"/1core_iops_125 >> data_result_same_time_"$2"_iops.dat
#cat at_same_time_execution_"$2"/w_agg_"$int_agg"/1core_iops_115 >> data_result_same_time_"$2"_iops.dat
    done
else
#for int_agg in 0x0000 0x0202 0x0303 0x0404 0x0505 0x4040
    for int_agg in 0x0000
    do
        cat at_same_time_execution_"$2"/w_agg_"$int_agg"/4core_latency_125 >> data_result_same_time_"$2"_latency.dat
        cat at_same_time_execution_"$2"/w_agg_"$int_agg"/4core_latency_115 >> data_result_same_time_"$2"_latency.dat
#cat at_same_time_execution_"$2"/w_agg_"$int_agg"/1core_latency_125 >> data_result_same_time_"$2"_latency.dat
#cat at_same_time_execution_"$2"/w_agg_"$int_agg"/1core_latency_115 >> data_result_same_time_"$2"_latency.dat
    done
fi
