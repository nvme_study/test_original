#!/bin/bash

for int_queue_depth in 1
#for int_queue_depth in 1 2 4 8 16 32 64
do
    mkdir at_same_time_execution_"$int_queue_depth"

    AGGREGATION_FACTOR=0x0000
    mkdir at_same_time_execution_"$int_queue_depth"/w_agg_$AGGREGATION_FACTOR
    
    ./setup_coalescing.sh 0 $AGGREGATION_FACTOR
    
    ./test_nice.sh $int_queue_depth
    
    mv [14]core* at_same_time_execution_"$int_queue_depth"/w_agg_$AGGREGATION_FACTOR
    mv result* at_same_time_execution_"$int_queue_depth"/w_agg_$AGGREGATION_FACTOR
    mv agg-* at_same_time_execution_"$int_queue_depth"/w_agg_$AGGREGATION_FACTOR
    mv top_result_* at_same_time_execution_"$int_queue_depth"/w_agg_$AGGREGATION_FACTOR
    mv cq_depth* at_same_time_execution_"$int_queue_depth"/w_agg_$AGGREGATION_FACTOR
    mv submit_count_* at_same_time_execution_"$int_queue_depth"/w_agg_$AGGREGATION_FACTOR
    
done
