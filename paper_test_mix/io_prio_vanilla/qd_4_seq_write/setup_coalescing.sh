#!/bin/bash

if (( $1 == 0 ))
then
echo "No Interrupt aggregation"
nvme set-feature /dev/nvme0 --feature-id=8 --value=0
nvme set-feature /dev/nvme0 --feature-id=9 --value=0x10000
nvme set-feature /dev/nvme0 --feature-id=9 --value=0x10001
nvme set-feature /dev/nvme0 --feature-id=9 --value=0x10002
nvme set-feature /dev/nvme0 --feature-id=9 --value=0x10003
nvme set-feature /dev/nvme0 --feature-id=9 --value=0x10004
nvme set-feature /dev/nvme0 --feature-id=9 --value=0x10005
nvme set-feature /dev/nvme0 --feature-id=9 --value=0x10006
nvme set-feature /dev/nvme0 --feature-id=9 --value=0x10007
elif (( $1 == 1 ))
then
echo "Interrupt aggregation :: $2"
nvme set-feature /dev/nvme0 --feature-id=8 --value=0
nvme set-feature /dev/nvme0 --feature-id=9 --value=0x10000
nvme set-feature /dev/nvme0 --feature-id=9 --value=0x10002
nvme set-feature /dev/nvme0 --feature-id=9 --value=0x10004
nvme set-feature /dev/nvme0 --feature-id=9 --value=0x10006

nvme set-feature /dev/nvme0 --feature-id=8 --value=$2
nvme set-feature /dev/nvme0 --feature-id=9 --value=1
nvme set-feature /dev/nvme0 --feature-id=9 --value=3
nvme set-feature /dev/nvme0 --feature-id=9 --value=5
nvme set-feature /dev/nvme0 --feature-id=9 --value=7
fi
