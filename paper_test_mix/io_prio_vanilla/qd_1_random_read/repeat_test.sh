#!/bin/bash

for in_repeat_id in {1..5}
do
	mkdir repeat_test_"$in_repeat_id"
	cp *.sh repeat_test_"$in_repeat_id"
	cd repeat_test_"$in_repeat_id"

	./auto_test_nice_same_time.sh
	./extract_result.sh 0 1
	./extract_result.sh 1 1

	cd ..
done

for in_repeat_id in {1..5}
do
	cat repeat_test_"$in_repeat_id"/data_result_same_time_1_iops.dat >> total_result_iops.dat
	cat repeat_test_"$in_repeat_id"/data_result_same_time_1_latency.dat >> total_result_latency.dat
done


