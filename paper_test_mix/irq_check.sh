#!/bin/bash

for ((index = 0 ; index < "$1"; index++))
do
    irq_num=`grep -wR nvme0q"$index" /proc/interrupts | awk -F: '{print $1}' | awk '{print $1}'`
    cat /proc/irq/"$irq_num"/smp_affinity_list
done

